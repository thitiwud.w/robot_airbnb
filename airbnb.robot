*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${url}  https://www.airbnb.com/  

*** Keywords ***
 Input Search Location
  SeleniumLibrary.Wait Until Page Contains Element  //*[@id="bigsearch-query-detached-query"]
  Input Text  //*[@id="bigsearch-query-detached-query"]  Newyork

 Click On Calendar
  Click Element  //*[@class="_uh2dzp"]

 Click Next Month
   Wait Until Element is Visible  //*[@class="_13tn83am"]
   Click Element  //*[@class="_13tn83am"]

 Click Check In Date Places to stay
   Wait Until Element is Visible  //*[@id="panel--tabs--0"]/div[1]/div/div/div/div[2]/div[2]/div/div[2]/div/table/tbody/tr[3]/td[7]/div/div
   Click Element  //*[@id="panel--tabs--0"]/div[1]/div/div/div/div[2]/div[2]/div/div[2]/div/table/tbody/tr[3]/td[7]/div/div
 
 Click Check Out Date Places to stay
   Wait Until Element is Visible  //*[@id="panel--tabs--0"]/div[1]/div/div/div/div[2]/div[2]/div/div[3]/div/table/tbody/tr[3]/td[6]/div/div
   Click Element  //*[@id="panel--tabs--0"]/div[1]/div/div/div/div[2]/div[2]/div/div[3]/div/table/tbody/tr[3]/td[6]/div/div

 Click Check In Date Experiences
   Wait Until Element is Visible  xpath=/html/body/div[4]/div/div[1]/div/div/div[1]/div[1]/div/header/div/div[2]/div[2]/div/div/div/form/div/div/div[3]/div[2]/div/section/div/div/div/div/div/div/div[2]/div[2]/div/div[2]/div/table/tbody/tr[3]/td[5]/div/div
   Click Element  xpath=/html/body/div[4]/div/div[1]/div/div/div[1]/div[1]/div/header/div/div[2]/div[2]/div/div/div/form/div/div/div[3]/div[2]/div/section/div/div/div/div/div/div/div[2]/div[2]/div/div[2]/div/table/tbody/tr[3]/td[5]/div/div
 
 Click Check Out Date Experiences
   Wait Until Element is Visible  xpath=/html/body/div[4]/div/div[1]/div/div/div[1]/div[1]/div/header/div/div[2]/div[2]/div/div/div/form/div/div/div[3]/div[3]/div/section/div/div/div/div/div/div/div[2]/div[2]/div/div[3]/div/table/tbody/tr[3]/td[3]/div/div
   Click Element  xpath=/html/body/div[4]/div/div[1]/div/div/div[1]/div[1]/div/header/div/div[2]/div[2]/div/div/div/form/div/div/div[3]/div[3]/div/section/div/div/div/div/div/div/div[2]/div[2]/div/div[3]/div/table/tbody/tr[3]/td[3]/div/div

 Click Date range
   Wait Until Element is Visible  //*[@id="flexible_date_search_filter_type-2"]/button
   Click Element  //*[@id="flexible_date_search_filter_type-2"]/button
 
 Click Add Guest
   Click Element  //*[text()="Add guests"]
 
 Click Add Adults
   Click Element  //*[@id="stepper-adults"]/button[2]
   Click Element  //*[@id="stepper-adults"]/button[2]
   Click Element  //*[@id="stepper-adults"]/button[1]
 
 Click Add 2 Children
   Click Element  //*[@id="stepper-children"]/button[2]
   Click Element  //*[@id="stepper-children"]/button[2]
   Click Element  //*[@id="stepper-children"]/button[2]
   Click Element  //*[@id="stepper-children"]/button[1]  
 
 Click Add infants
   Click Element  //*[@id="stepper-infants"]/button[2]
   Click Element  //*[@id="stepper-infants"]/button[2]
   Click Element  //*[@id="stepper-infants"]/button[1]
 
 Click Search Button
   Click Element  //*[@class="_163rr5i"]
 
 Check Result Of Search Places to stay
   Wait Until Element is Visible  //*[@id="ExploreLayoutController"]/div[1]/div[1]/div[1]/div[1]/section/div[2]
   Page Should Contain Element  //*[@id="ExploreLayoutController"]/div[1]/div[1]/div[1]/div[1]/section/div[2]

 Check Result Of Search Experiences
   Wait Until Element is Visible  //*[@id="FMP-target"]/div/div
   Page Should Contain Element  //*[@id="FMP-target"]/div/div

 Click I'm flexible
   Wait Until Element is Visible  //*[@id="tab--tabs--1"]
   Click Element  //*[@id="tab--tabs--1"]
 
 Click Stay for a Month
   Wait Until Element is Visible  //*[@id="flexible_trip_lengths-one_month"]/button
   Click Element  //*[@id="flexible_trip_lengths-one_month"]/button

 Click On Experiences
   Click Element  xpath=/html/body/div[4]/div/div[1]/div/div/div[1]/div[1]/div/header/div/div[2]/div[2]/div/div/div/form/fieldset/div/label[2]/span

*** Test Cases ***
Search via Places to stay Fixed Calendar
  Open Browser  ${url}  gc
  Maximize Browser Window
  sleep  2s
  Input Search Location
  sleep  2s
  Click On Calendar
  sleep  2s
  Click Next Month
  sleep  2s
  Click Check In Date Places to stay
  sleep  2s
  Click Check Out Date Places to stay
  sleep  2s
  Click Date range
  sleep  2s
  Click Add Guest
  sleep  2s
  Click Add Adults
  sleep  2s
  Click Add 2 Children
  sleep  2s
  Click Add infants
  sleep  2s
  Click Search Button
  sleep  2s
  Check Result Of Search Places to stay
  sleep  2s
  Close Browser

Search via Places to stay Flexible Calendar
  Open Browser  ${url}  gc
  Maximize Browser Window
  sleep  2s
  Input Search Location
  sleep  2s
  Click On Calendar
  sleep  2s
  Click I'm flexible
  sleep  2s
  Click Stay for a Month
  sleep  2s
  Click Add Guest
  sleep  2s
  Click Add Adults
  sleep  2s
  Click Add 2 Children
  sleep  2s
  Click Add infants
  sleep  2s
  Click Search Button
  sleep  2s
  Check Result Of Search Places to stay
  sleep  2s
  Close Browser

Search via Experiences
  Open Browser  ${url}  gc
  Maximize Browser Window
  sleep  2s
  Click On Experiences
  Input Search Location
  sleep  2s
  Click On Calendar
  sleep  2s
  Click Next Month
  sleep  2s
  Click Check In Date Experiences
  sleep  2s
  Click Check Out Date Experiences
  sleep  2s
  Click Search Button
  sleep  2s
  Check Result Of Search Experiences
  sleep  2s
  Close Browser